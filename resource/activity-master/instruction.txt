Activity:
s11 Instruction:
  - apply BS
		- Components
		- Grid
		- Flex
  
1. In the S11 folder, create an activity folder.
2. Clone/copy the template for the index.html file.
3. Apply Bootstrap to the project by adding all the dependencies.
4. Apply Bootstrap classes to the following:
	- Navbar
	- Footer
	- About Me Section
	- Projects Section
5. Apply Bootstrap classes to the outer div tag to create a fluid container, center the text elements and add a padding.
6. Apply Bootstrap classes to the Web API Ecommerce and React Frontend Section with the following layout:
	- When in a large screen, the contents of the columns at each row should be alternating.
	- At first row, the left side contains the text and the right side contains the image.
	- At second row, the left side contains the image and the right side contains the text.
	- When in a small screen, the image should be shown first before the text.
7. Apply Bootstrap classes to the Other Projects Section with the following layout:
	- The images and their corresponding headers and texts should all appear in a column.
	- Each set of images and texts should be equal in width and should appear beside each other
8. Apply Bootstrap classes to the Navbar Section with the following layout:
	- The elements must appear side by side with a space between the name of the developer and the links to the other pages.
9. Apply Bootstrap classes to the outer div tag of the Main Content Section to  create a fluid container and add a padding.
10. Apply Bootstrap classes to the Other Project Section with the following layout:
	- When in a medium screen or larger screen, the contents should appear side by side.
	- When in a small screen, the contents should appear on top of each other.
11. Apply Bootstrap classes to the Core Tools Section with the following layout:
	- The contents must form a triangular shape on all screen sizes.
12. Apply Bootstrap classes to the Other Tools Section with the following layout:
	- When in a medium screen or larger screen, the contents should appear side by side in 2 rows.
	- When in a small screen, the contents should appear side by side in 2 columns.
13. Create a git repository named S11.
14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
15. Add the link in Boodle.

    Stretch Goal:
	- Add Footer
	- Link Projects to your Projects
		- Project
		- Other Project
	- Link Tools
		- Core Tools 
		- Other Tools
